var socketModule = (function (){
    var socket, chat = '', selectedRoom;
    
	var startSocket = function(type){
      socket = io.connect('https://localhost:3443');
        
      socket.on('connect', function(){
		socket.emit('newuser', {username: user.username, screen: type});
        console.log('connected');
        socketInited = true;  
	  });	     
		
	  socket.on('rooms', function(data){
        $('.add-rooms span').hide();
		updateRooms(data.rooms, data.myrooms, data.user);
        user.allRooms = data.rooms;
        user.myrooms = data.myrooms;  
        if(!videoInited){ 
          for(var r in user.myrooms){
                        if(user.myrooms[r].title==$('#chatName').text()){
                            if(user.myrooms[r].admin){
                                startVideo('admin', $('#chatName').text());
                                videoInited = true;
                                break;
                            } else {
                                startVideo('user', $('#chatName').text());
                                videoInited = true;
                                break;
                            }
                        }
                    }
        }
          
	  });

	  socket.on('updatechat', function(user, msg){
	  	updateChat(user, msg);
	  });    
        
      socket.on('roomexist', function(){
          $('.add-rooms span').show();
      });    
        
      socket.on('userexist', function(){
          $('.add-users span').text('User already joined.');
          $('.add-users span').show();
      });  
        
      socket.on('usernotexist', function(){
          $('.add-users span').text('There is no user with this username.');
          $('.add-users span').show();
      });    
        
      socket.on('alreadyjoin', function(title){
          $('.room-button').each(function(){
              if($(this).children('.room-title').text()==title){
                  $(this).children('span').show();
              }
          });
      });
        
      socket.on('allowToUser', function(username){
          if(username==user.username){
              showModal();
          }
      });   
        
      socket.on('removeStream', function(id, username){
          if(user.username!=username){
              rtc.removePeer(id);
          } else {
              $('#l-video>video').remove();
              localVideoStream.stop();
          }
      });      
        
      socket.on('userAcceptStream', function(roomT, username){
          adminIDChanged = true;
      });  
//        
//      socket.on('userDeclineStream', function(roomT, username){
//
//      });     
        
      socket.on('logout', function(){
          logout();
      });    
        
      socket.on('disconnect', function(){
          if(typeof deleteUsername == 'function')
            deleteUsername();
      });    
    }; 	

	  var addRoom = function(){
	  	selectedRoom = $('#title').val();
		socket.emit('addChat', {title: $('#title').val()});
		$('.room-text').html('');
	  };

	  var deleteRoom = function(e){
	   var roomT = $(e.target).prev().prev().text();
		socket.emit('removeChat', {title: roomT});
	  };
    
      var joinRoom = function(e){
          var roomT = $(e.target).prev().text();
          socket.emit('joinChat', {title: roomT});
      };
    
      var leftRoom = function(e){
          var roomT = $(e.target).prev().prev().text();
          socket.emit('leftChat', roomT);
      };
    
      var openRoom = function(e){
          var roomT = $(e.target).prev().text();
          window.open('/chats/' + roomT, '_blank');
      };

	  var selectRoom = function(title){
		socket.emit('changeRoom', title);
	  };

	  var sendMessage = function(){
	  	var text = $('#message').val();
	  	socket.emit('sendMessage', {messageText: text, user: user.username, date: new Date()});
        $('#message').val('');
	  };
    
      var addUsers = function(){
          var username = $('#username').val();
          if(username){
              $('.add-users span').text('');
              $('.add-users span').hide();
              socket.emit('addUserToRoom', username);
          }
      };    
    
      var allowVideo = function(e){
          var username = $(e.target).prev().text();
          socket.emit('allowVideo', username, title);
          $(e.target).next().show();
          $(e.target).hide();
          $('.user-allow').addClass('hidden');
      };   

      var updateUsers = function(){
          var title = $('#chatName').text(), template = '', keyS = false;

          for(var r in user.allRooms){
              if(user.allRooms[r].title == title){
                  keyS = false;
                  for(var s in user.allRooms[r].streams){
                      if(user.allRooms[r].streams[s]){
                          keyS = true;
                          break;
                      }
                  }
                  
                  for(var u in user.allRooms[r].users){
                      if(user.allRooms[r].admin == user.allRooms[r].users[u])
                        template += "[A]<span>" + user.allRooms[r].users[u] + "*</span>";
                      else
                        template += "<span data-username='"+user.allRooms[r].users[u]+"'>" + user.allRooms[r].users[u] + "</span>";
                      
                      if(user.allRooms[r].admin == user.username && user.allRooms[r].admin != user.allRooms[r].users[u]){
                          if(user.allRooms[r].streams[user.allRooms[r].users[u]]=='pending')
                            template += "<a class='user-allow hidden' href='#' onclick='socketModule.allowVideo(event)'>allow video</a><span class='user-pend'>pending</span><span class='user-stream hidden'>streaming</span>";
                          else if(user.allRooms[r].streams[user.allRooms[r].users[u]]=='streams')
                            template += "<a class='user-allow hidden' href='#' onclick='socketModule.allowVideo(event)'>allow video</a><span class='user-pend hidden'>pending</span><span class='user-stream'>streaming<a class='stop-button' href='#' onclick='socketModule.stopStream(event)'>stop</a></span>";
                          else if(!keyS)
                              template += "<a class='user-allow' href='#' onclick='socketModule.allowVideo(event)'>allow video</a><span class='user-pend hidden'>pending</span><span class='user-stream hidden'>streaming</span>";
                          else 
                              template += "<a class='user-allow hidden' href='#' onclick='socketModule.allowVideo(event)'>allow video</a><span class='user-pend hidden'>pending</span><span class='user-stream hidden'>streaming</span>";
                          
                      } else if(user.allRooms[r].admin != user.username && user.allRooms[r].admin != user.allRooms[r].users[u]){
                          if(user.allRooms[r].streams[user.allRooms[r].users[u]]=='streams')
                            template += "<span class='user-stream'>streaming</span>";
                      }
                      
                        if(user.allRooms[r].users.length-1>u)
                            template += '|';
                  }
                  
                  if(user.allRooms[r].admin == user.username){
                      $('.add-users').show();
                  }
                  break;
              }
          }
          
          $('.userNames p').html(template);
          
      };
    
      var addAdminID = function(id, title){
        socket.emit('saveAdmin', id, title);  
      };
    
      var acceptStream = function(title, user){
          socket.emit('streamAccepted', title, user);
      };
    
      var declineStream = function(title, user){
          socket.emit('streamDeclined', title, user);
      };
    
      var stopStream = function(e){
            var username = $(e.target).parent().prev().prev().prev().text();
            var peer = $('#r-video>video').data('peer');
            var id = $('#r-video>video').data('id');
            rtc.removePeer(peer);
          
            socket.emit('stopStream', username, peer, title, id);
      };

	  function updateChat(user, msg){
		$('.allchats').append("<p><b>"+user+":</b> "+msg+"</p>");
	  }
    
      function initChat(chat){
          var cDate, template = '';
          
          for(var r in user.myrooms){
              if(user.myrooms[r].title==chat.title){
                  cDate = user.myrooms[r].date;
                  break;
              }
          }
          
          for(var c in chat.chat){
              if((new Date(chat.chat[c].date)).getTime()>=(new Date(cDate)).getTime()){
                  if(chat.chat[c].message.indexOf('SERVER')==-1 && chat.chat[c].message.indexOf(user.username)==-1)
                    template += "<p><b>" + chat.chat[c].user + ":</b> " + chat.chat[c].message + "</p>";
              }
          }
          
          $('.allchats').html(template);
      }    

	  function updateRooms(rooms, myrooms, username){
        
		var temp = '', mTemp = '';
		for(var i in rooms){
            if(rooms[i].admin==user.username)
			     temp += "<li class=room-button><span>[A] </span><span class='room-title'>"+rooms[i].title+"</span><a></a><a href='#' onclick='socketModule.deleteRoom(event)'>delete</a></li>";
            else 
                temp += "<li class=room-button><span class='room-title'>"+rooms[i].title+"</span><a href='#' onclick='socketModule.joinRoom(event)'>join</a><span class='join-error'>You already joined</span></li>";
		}
        
        if(username==user.username)  
            for(var i in myrooms){
                if(myrooms[i].admin)
                    mTemp += "<li class=myroom-button><span>[A] </span><span>"+myrooms[i].title+"</span><a href='#' onclick='socketModule.openRoom(event)'>open</a><a href='#' onclick='socketModule.deleteRoom(event)'>delete</a></li>";
                else
                    mTemp += "<li class=myroom-button><span>"+myrooms[i].title+"</span><a href='#' onclick='socketModule.openRoom(event)'>open</a><a href='#' onclick='socketModule.leftRoom(event)'>left</a></li>";
            }  

		mTemp += "<div class='add-rooms'><input name='title' id='title' type='text' placeholder='Enter title of room'><button type='button' onclick='socketModule.addRoom()'>Add</button><span>Room already exist</span></div>";

	  	$('.roomNames').html(temp); 
        if(username==user.username)
            $('.yourRoomNames').html(mTemp);  
	  }
    
    
    setInterval(function(){
        updateUsers();
    }, 5000);
    
     return {
         socketInfo: socket,
         start: startSocket,
         addRoom: addRoom,
         selectRoom: selectRoom,
         deleteRoom: deleteRoom,
         joinRoom: joinRoom,
         leftRoom: leftRoom,
         openRoom: openRoom,
         sendMessage: sendMessage,
         addUsers: addUsers,
         allowVideo: allowVideo,
         updateUsers: updateUsers,
         addAdminID: addAdminID,
         acceptStream: acceptStream,
         declineStream: declineStream,
         stopStream: stopStream
     };
    })();