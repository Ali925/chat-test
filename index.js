var express = require('express');
var app = express();
var fs = require('fs');
var privateKey  = fs.readFileSync('server.key', 'utf8');
var certificate = fs.readFileSync('server.crt', 'utf8');
var credentials = {key: privateKey, cert: certificate};
var https = require('https').createServer(credentials, app);
var io = require('socket.io')(https);
var session = require('express-session');

var rooms = [], users = {}, roomsP = [];

app.use(session({
  secret: 'multichat',
  resave: true,
  saveUninitialized: true
}));

app.set('view engine', 'ejs');
app.use("/", express.static(__dirname + "/"));

app.get('/', function(req, res){
  res.sendFile(__dirname + '/views/index.html');
});

app.get('/chats/:title', function(req, res){
   var title = req.params.title, roomData, username = req.session.username, userHas = false, cDate;
    for(var r in rooms){
        if(rooms[r].title==title){
            roomData = rooms[r];
            break;
        }
    }

    if(roomData && username){
        
        for(var u in roomData.users){
            if(roomData.users[u] == username){
                userHas = true;
                break;
            }
        }
        
        for(var r in users[username].rooms){
              if(users[username].rooms[r].title==roomData.title){
                  cDate = users[username].rooms[r].date;
                  break;
              }
        }
        
    } else if(!username) {
        res.render('error', {
            error: 'You should login first!'
        });           
    } else if(!roomData){
        res.render('error', {
            error: 'Ops.. there no room like this!'
        });
    }
    
    if(userHas && roomData){
        res.render('chat', {
                title: title,
                roomData: roomData,
                cDate: cDate,
                username: username
            });
    } else if(!userHas && username && roomData){
        res.render('error', {
            error: "You haven't permission to this room!"
        });
    }

});

app.get('/login', function(req, res){
    var keyU = true;

    for(var u in users){
        if(u==req.query.name){
            keyU = false;
            break;
        }
    }
    
    if(keyU){
        users[req.query.name] = {rooms: []};
        req.session.username = req.query.name;
        res.send(true);
    } else {
        req.session.username = req.query.name;
        res.send(false);
    }
});

app.get('/logout', function(req, res){
    req.session.username = null;
    res.send(true);
});

io.sockets.on('connection', function (socket) {

  socket.on('newuser', function(data){
      
    if(data.screen=='main'){
        var keyU = true;

        for(var u in users){
            if(u==data.username){
                keyU = false;
                break;
            }
        }

        if(keyU)
            socket.emit('logout');
        
    }  
      
    socket.username = data.username;    
    sendRooms();
  });    
    
  socket.on('addChat', function(data){
    var keyC = true;
      
    for(var i in rooms){
        if(rooms[i].title==data.title){
            keyC=false;
            break;
        }
    }  
    
    if(keyC){
        rooms.push({title: data.title, chat: [], users: [], streams: {}, stopped: []});
        
        io.sockets.in(data.title).emit('updatechat', 'SERVER', socket.username + ' has joined to this room');
        saveChat(data.title, 'SERVER', socket.username + ' has joined to this room', new Date());
        
        for(var r in rooms){
            if(rooms[r].title==data.title){
                rooms[r].users.push(socket.username);
                rooms[r].admin = socket.username;
                break;
            }
        }
        
        setTimeout(function(){
            users[socket.username].rooms.push({title:data.title, date: new Date(), admin: true});

            sendRooms();    
        },500);    

    } else {
       socket.emit('roomexist'); 
    }
    
  });    
    
  socket.on('joinChat', function(name){
    var userN = socket.username, keyR = true;  
      
    for(var r in users[userN].rooms){
        if(users[userN].rooms[r].title==name.title){
            keyR = false;
            break;
        }
    }    
      
    if(keyR){  
        io.sockets.in(name.title).emit('updatechat', 'SERVER', userN + ' has joined to this room');
        saveChat(name.title, 'SERVER', userN + ' has joined to this room', new Date());
        
        for(var r in rooms){
            if(rooms[r].title==name.title){
                rooms[r].users.push(userN);
                break;
            }
        }
        
        setTimeout(function(){
            users[userN].rooms.push({title:name.title, date: new Date(), admin: false});

            sendRooms();    
        },500);
        
    } else {
        socket.emit('alreadyjoin', name.title);
    }
  });    
    
  socket.on('leftChat', function(title){
        var userN = '';
      
        for(var u in users){
            if(socket.username==u){
                userN = u;
                break;
            }
        }  
      
        for(var r in users[userN].rooms){
            if(users[userN].rooms[r].title==title){
                users[userN].rooms.splice(r, 1);
                break;
            }
        }  
      
        for(var r in rooms){
            if(rooms[r].title==title){
                for(var u in rooms[r].users){
                    if(rooms[r].users[u] == userN){
                        rooms[r].users.splice(u, 1);
                        break;
                    }
                }
                break;
            }
        }
      
        socket.leave(title);
        io.sockets.in(title).emit('updatechat', 'SERVER', socket.username + ' has left this room');
        saveChat(title, 'SERVER', socket.username + ' has left this room', new Date());
      
        sendRooms();
      
  });      
    
  socket.on('addUserToRoom', function(username){
      var cRoom, kUser = false, now = new Date();
      
      for(var u in users){
          if(u == username){
              kUser = true;
              break;
          }
      }
      
      if(kUser){
          for(var r in rooms){
              if(rooms[r].title==users[socket.username].currentRoom){
                  cRoom = r;
                  break;
              }
          }

          for(var u in rooms[cRoom].users){
              if(rooms[cRoom].users[u] == username){
                  kUser = false;
                  break;
              }
          }

          if(kUser){
                rooms[cRoom].users.push(username);

                setTimeout(function(){
                    
                    io.sockets.in(rooms[cRoom].title).emit('updatechat', 'SERVER', username + ' has joined to this room');
                    saveChat(rooms[cRoom].title, 'SERVER', username + ' has joined to this room', now);
                    
                    users[username].rooms.push({title:rooms[cRoom].title, date: new Date(), admin: false});
                    
                    io.emit('rooms', {rooms: rooms, myrooms: users[username].rooms, user: username});
  
                },500);
              
          } else {
              socket.emit('userexist');
          }
      } else {
          socket.emit('usernotexist');
      }
  });    

  socket.on('removeChat', function(data){
    for(var i in rooms){
      if(data.title==rooms[i].title){
        rooms.splice(i, 1);
        break;
      }
    }

    for(var t in io.sockets.adapter.rooms){
      if(data.title == t){
        delete io.sockets.adapter.rooms[t];
        break;
      }
    }
      
    for(var u in users){
        for(var r in users[u].rooms){
            if(users[u].rooms[r].title==data.title){
                users[u].rooms.splice(r, 1);
                break;
            }
        }
    }  

    sendRooms();
  });

  socket.on('sendMessage', function(data){
    io.sockets.in(socket.room).emit('updatechat', data.user, data.messageText);
    saveChat(socket.room, data.user, data.messageText, new Date());
  });

  socket.on('changeRoom', function(title){

    if(users[socket.username].currentRoom && users[socket.username].currentRoom != title){  
        socket.room = title;

        users[socket.username].currentRoom = title;  

        socket.join(title);
    } else {
        users[socket.username].currentRoom = title; 
        socket.room = title;
        socket.join(title);
    }
  });
    
  socket.on('allowVideo', function(username, title){
      for(var r in rooms){
        if(rooms[r].title==title){
            rooms[r].streams[username] = 'pending';
            break;
        }  
      }
      
      io.emit('allowToUser', username);
      sendRooms();
  });    

  socket.on('saveAdmin', function(id, title){
      for(var r in rooms){
        if(rooms[r].title==title){
            rooms[r].orgVideoId = id;
            break;
        }
      }
      
      sendRooms();
  });
    
  socket.on('streamAccepted', function(title, user){
      
      for(var r in rooms){
        if(rooms[r].title==title){
            rooms[r].streams[user] = 'streams';
            break;
        }  
      }
      
      io.sockets.in(title).emit('userAcceptStream', title, user);
      sendRooms();
  });    
    
  socket.on('streamDeclined', function(title, user){
      for(var r in rooms){
        if(rooms[r].title==title){
            rooms[r].streams[user] = null;
            break;
        }  
      }
      
      //io.sockets.in(title).emit('userDeclineStream', title, user);
      sendRooms();
  });     
    
  socket.on('stopStream', function(username, peer, title, id){
      for(var r in rooms){
        if(rooms[r].title==title){
            rooms[r].streams[username] = null;
            rooms[r].stopped.push(id);
            break;
        }  
      }
      
      io.sockets.in(title).emit('removeStream', peer, username);
      sendRooms();
  });     
    
  socket.on('disconnect', function(){
    //socket.leave(socket.room);
  });

  function sendRooms(){
    if(users[socket.username])  
      io.emit('rooms', {rooms: rooms, myrooms: users[socket.username].rooms, user: socket.username});
  }
    
  function saveChat(room, user, message, date){
      for(var r in rooms){
          if(rooms[r].title==room){
              rooms[r].chat.push({user: user, message: message, date: date});
              break;
          }
      }
  }    

});

https.listen(3443, function(){
  console.log('listening on *:3443');
});